#include <module.h>
#include <dryos.h>
#include <property.h>
#include <bmp.h>
#include <shoot.h>
#include <lens.h>
#include <zebra.h>

unsigned int testplug_init()
{
    printf("testplug: Initialized\n");

    return 0;
}

unsigned int testplug_deinit()
{
    return 0;
}

#define SHOT_INTERVAL 20
#define SHOT_OFFSET 29

unsigned int second_timer(unsigned int ctx)
{
    static int counter = 0;
    
    counter++;
    //bmp_printf( FONT(FONT_MED, COLOR_WHITE, COLOR_BLACK), 30, 20, "seconds: %d", counter);

    if(counter % SHOT_INTERVAL == 0 && counter > SHOT_OFFSET) {
        lens_set_rawshutter(SHUTTER_1_4000);
        lens_take_picture(0,0);
    }
    else if(counter % SHOT_INTERVAL == 3 && counter > SHOT_OFFSET) {
        lens_set_rawshutter(SHUTTER_1_2000); // 1/2000
        lens_take_picture(0,0);
    }
    else if(counter % SHOT_INTERVAL == 6 && counter > SHOT_OFFSET) {
        lens_set_rawshutter(SHUTTER_1_1000); // 1/1000
        lens_take_picture(0,0);
    }
    else if(counter % SHOT_INTERVAL == 9 && counter > SHOT_OFFSET) {
        lens_set_rawshutter(SHUTTER_1_500); // 1/500
        lens_take_picture(0,0);
    }
    else if(counter % SHOT_INTERVAL == 12 && counter > SHOT_OFFSET) {
        lens_set_rawshutter(SHUTTER_1_250); // 1/250
        lens_take_picture(0,0);
    }
    else if(counter % SHOT_INTERVAL == 15 && counter > SHOT_OFFSET) {
        idle_force_powersave_now();
    }

    return 0;
}


MODULE_INFO_START()
    MODULE_INIT(testplug_init)
    MODULE_DEINIT(testplug_deinit)
MODULE_INFO_END()

MODULE_CBRS_START()
    MODULE_CBR(CBR_SECONDS_CLOCK, second_timer, 0)
MODULE_CBRS_END()

MODULE_PARAMS_START()
MODULE_PARAMS_END()

MODULE_PROPHANDLERS_START()
MODULE_PROPHANDLERS_END()
