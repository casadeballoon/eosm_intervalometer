static char __module_string_a_name [] MODULE_STRINGS_SECTION = "Name";
static char __module_string_a_value[] MODULE_STRINGS_SECTION = "HAB Sunrise Intervalometer";
static char __module_string_b_name [] MODULE_STRINGS_SECTION = "Author";
static char __module_string_b_value[] MODULE_STRINGS_SECTION = "Vimal Bhalodia";
static char __module_string_c_name [] MODULE_STRINGS_SECTION = "License";
static char __module_string_c_value[] MODULE_STRINGS_SECTION = "MIT";
static char __module_string_d_name [] MODULE_STRINGS_SECTION = "Summary";
static char __module_string_d_value[] MODULE_STRINGS_SECTION = "Sunrise intervalometer";
static char __module_string_e_name [] MODULE_STRINGS_SECTION = "Description";
static char __module_string_e_value[] MODULE_STRINGS_SECTION = 
    "A hard-coded intervalometer for sunrise launches with\n"
    "fixed-focus fixed-aperture lenses\n"
    "\n"
;
static char __module_string_f_name [] MODULE_STRINGS_SECTION = "Last update";
static char __module_string_f_value[] MODULE_STRINGS_SECTION = 
    "3c58417 on 2014-02-18 11:42:58 UTC by alex:\n"
    "Renamed testmodule to testmod to fit the 8.3 name limit\n"
;
static char __module_string_g_name [] MODULE_STRINGS_SECTION = "Build date";
static char __module_string_g_value[] MODULE_STRINGS_SECTION = "2014-09-13 20:39:20 UTC";
static char __module_string_h_name [] MODULE_STRINGS_SECTION = "Build user";
static char __module_string_h_value[] MODULE_STRINGS_SECTION = 
    "magiclantern@magiclantern-VirtualBox\n"
    "\n"
;

MODULE_STRINGS_START()
    MODULE_STRING(__module_string_a_name, __module_string_a_value)
    MODULE_STRING(__module_string_b_name, __module_string_b_value)
    MODULE_STRING(__module_string_c_name, __module_string_c_value)
    MODULE_STRING(__module_string_d_name, __module_string_d_value)
    MODULE_STRING(__module_string_e_name, __module_string_e_value)
    MODULE_STRING(__module_string_f_name, __module_string_f_value)
    MODULE_STRING(__module_string_g_name, __module_string_g_value)
    MODULE_STRING(__module_string_h_name, __module_string_h_value)
MODULE_STRINGS_END()
