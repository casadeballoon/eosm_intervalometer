HAB Sunrise Intervalometer
==========================

A hard-coded intervalometer for sunrise launches with fixed-focus fixed-aperture lenses

:Author: Vimal Bhalodia
:License: MIT
:Summary: Sunrise intervalometer

